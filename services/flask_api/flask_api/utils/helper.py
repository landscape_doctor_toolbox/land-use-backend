# Helper functions for recommendation calculations
import rasterio
import numpy
import json
import sys, os

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "../..")))
from flask_api.routes import db, text
from timeit import default_timer
from multiprocessing import Pool


# queries data from the database and returns the feature data
def get_feature_data(lon, lat):
    query = text("SELECT * FROM rasters.get_feature_values(:lon, :lat)")
    feature_values= db.engine.execute(query, lon=lon, lat=lat).fetchall()
    result = {
        "agro_climatic_zone": "nodata",
        "elevation": "nodata",
        "land_form": "nodata",
        "precipitation": "nodata",
        "slope_shape": "nodata",
        "soil_depth": "nodata",
        "soil_permeability":"nodata",
        "soil_texture": "nodata",
        "topographic_slope": "nodata" 
    }

    # If there are data point returned from the database (i.e lon, lat are in bounds)
    if (len(feature_values) >= 7): # number of parameters stored in the database
        for i in range(len(feature_values)):
            if (feature_values[i][1] is None):
                result[feature_values[i][0]] = "nodata"
            else:
                try:
                    result[feature_values[i][0]] = float(feature_values[i][1])
                except:
                    result[feature_values[i][0]] = feature_values[i][1]
    else:
        result = {}
    print("\nfeature_value: ", feature_values)
    print("decoded result: ", result)
    return result

# converts string recommendation name to a value between 0 - 255 
def rec_to_value(recommendation, recom_path, RECOMMENDATIONS):
    if(RECOMMENDATIONS is None):
        RECOMMENDATIONS = {}
        with open(recom_path) as f:
            recommendations_list = list(json.load(f)["data"].keys())
        # dividing 0-255 into len(recommendations_list) 
        # to create distict-ness when overlaying the tiff file over the map
        # this can be removed later
        for i in range(len(recommendations_list)):
            RECOMMENDATIONS[recommendations_list[i]] = int(255*(1-(i/len(recommendations_list))))

    return RECOMMENDATIONS[recommendation], RECOMMENDATIONS

# opens the raster file using rasterio and returns the numpy array
#  and other relevant informations about the raster
def read_raster(raster_path):
    try:
    # opens the raster file to be read 
        with rasterio.open(raster_path, "r") as source_raster:
            raster_profile = source_raster.profile
            raster_grid = source_raster.read() # returns a numpy array
            raster_transform = source_raster.transform
        return raster_grid, raster_transform, raster_profile

    except:
        print("File not found!")
        exit(1)

# write the recommendation raster to the path given by save_path variable
def write_raster(raster_grid, raster_profile, save_path):
    # Rewrite raster file with updated values
    try:
        # updating nodata value on profile
        raster_profile["nodata"] = 0
        raster_profile["dtype"] = "uint8"
        with rasterio.open(save_path, 'w', **raster_profile) as dst:
            dst.write(raster_grid.astype(rasterio.uint8))
    except:
        print("Couldn't write results to file!")
        exit(1)

def compute_grid(raster_grid, grid_number, centroid_transform, raster_profile, nodata, recom_path, range_path):
    # lambda function
    transform_row_col = lambda c, r: centroid_transform * (c, r)
    RECOMMENDATION = None
    for x in range(raster_grid.shape[0]):
        for y in range(raster_grid.shape[1]):
            
            y_shifted = raster_grid.shape[1]*grid_number + y
            start_time = default_timer()
            if raster_grid[x][y] == raster_profile["nodata"]:
                raster_grid[x][y] = nodata
                continue
            (lon, lat) = transform_row_col(y_shifted, x)
            # call get_feature_data to get features data value from the database 
            # for example, feature data = {"elevation": 123, "precipitation": 234 ...}
            features_data = get_feature_data(float(lon), float(lat))
            print("Lon: ",lon," Lat: ",lat)
            print("Yshifted: ", y_shifted, "Y: ", y," X: ",x)
            print("grid elevation value: ", raster_grid[x][y])
            if (features_data == {}) or (len(features_data) != 9):
                raster_grid[x][y] = nodata
                continue
            # call recommed function to calculate the recommendation technique 
            # based on the information provided in the recommendation and range
            # files and feature_data variable
            recommendation = recommend(features_data, recom_path, range_path)
            # converts the recommendation name to integer value between 0-255
            recommendation_value, RECOMMENDATION = rec_to_value(recommendation, recom_path, RECOMMENDATION)
            # assign the recommendation value to the cell
            raster_grid[x][y] = recommendation_value
            elapsed = default_timer() - start_time
            print("\ntime:  ", elapsed, "\n")

    return raster_grid

# function for creating recommendation file
def create_recommendation_file(raster_path, recom_path, range_path):
    # no data cells in the recommendation raster will be assigned value 0
    nodata = 0
    raster_grid, raster_transform, raster_profile = read_raster(raster_path)
    
    # get centroid transform
    centroid_transform = raster_transform * rasterio.Affine.translation(0.5, 0.5) 
    # reshape raster grid by selecting band 0 (the only band) to allow spliting
    raster_grid = raster_grid.reshape((raster_grid.shape[1], raster_grid.shape[2]))
    
    # split grid into 15 equal parts
    try:
        grid_subsections = numpy.split(raster_grid, 15, axis = 1)
    except ValueError: # if array cannot be split into 15 parts
        grid_subsections = [raster_grid]

    # prepare arguments for each job
    job_args = [(grid_subsection, grid_number, centroid_transform, raster_profile, nodata, recom_path, range_path) for grid_number, grid_subsection in enumerate(grid_subsections)]
    
    # use multiprocessing to compute the grids
    with Pool(processes = 5) as pool:
        grid_results = pool.starmap(compute_grid, job_args)
    
    try:
        raster_grid = numpy.concatenate(grid_results, axis = 1)
    except AxisError: # if the array was not split
        raster_grid = grid_results

    # reshape the grid back to its former dimensions
    raster_grid = raster_grid.reshape((1, raster_grid.shape[0], raster_grid.shape[1]))
    # writing raster_grid 
    write_raster(raster_grid, raster_profile, raster_path)

def check(value, feature, feature_val):
    if(feature in ["agro_climatic_zone", "soil_texture"]):
        return value == feature_val
    elif(feature == "land_form"):
        return value in feature_val
    else:
        if(feature_val[0] == "i"):
            return value <= feature_val[1]
        elif(feature_val[1] == "i"):
            return value >= feature_val[0]
        else:
            return value >= feature_val[0] and value <= feature_val[1]

# feature_data is the feature (value, pair) dictionary returned from the api
# feature_to_index function takes the feature_data and convertes to index value
def feature_to_index(features_data, range_path):
    feature_index_pairs = {}
    # load feature ranges json data
    try:
        with open(range_path) as f:
            feature_ranges = json.load(f)["data"]
    except:
        print("feature ranges File Not found")
        exit(1)
    print("feature_data", features_data)
    for feature, value in features_data.items():  
        current_feature = feature_ranges[feature]
        found = False
        # The value for a feature should not be -1 or invalid
        if(value != "nodata"):
            for i in range(len(current_feature)):
                # All the conditions that needs to be satisfied 
                # for a feature to belong in a speicifc index range
                if(check(value, feature, current_feature[i])):
                    found = True
                    feature_index_pairs[feature] = i
                    break
        if(value == "nodata" or (not found)) :
            feature_index_pairs[feature]= -1
    return feature_index_pairs

# feature_data is the feature, value pair dictionary returned from the api 
# for instance {"elevation": 123, "precipitation": 234 ...}
def recommend(features_data, recom_path, range_path):
    feature_index_pairs = feature_to_index(features_data, range_path)
    recom_dist_pair = {}
    try:
        with open(recom_path) as f:
            recommendations = json.load(f)["data"]
    except:
        print("recommendation json File Not Found")
        exit(1)
    # iterates through the recommendations and calculates hamming distance
    # for each recommendation
    for recom, features in recommendations.items():
        recom_dist_pair[recom] = 0
        for feature, bin_list in features.items():
            index = feature_index_pairs[feature]
            if(index < 0 or bin_list[index] != 1): 
                recom_dist_pair[recom] += 1
    return min(recom_dist_pair, key = recom_dist_pair.get)           
