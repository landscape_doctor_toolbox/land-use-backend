# utility function tests
from flask_api.utils import helper
import os
from os import environ
import rasterio
import numpy as np
import pytest


# tests for each function in utils/helper.py
# test getting feauture data from the db
def test_get_feature_data():
    lon = 36.73
    lat = 8.95
    response = helper.get_feature_data(lon, lat)
    assert response["elevation"] == 1464.0
    assert response["precipitation"] == 1523.0534201152623
    assert response["soil_texture"] == "silt"
    assert response["topographic_slope"] == 89.98686981201172
    assert response["land_form"] == 8
    assert response["slope_shape"] == -2.567535193520598e-05
    assert response["agro_climatic_zone"] == "Wet Kolla" 
    assert response["soil_permeability"] == "nodata"
    assert response["soil_depth"] == "nodata"
    
# tests converting recommendation name to value
def test_rec_to_value():
    recommendation = "Bench terrace"
    recom_path = environ.get("RECOMMENDATION_FILE")
    expected_val = 255
    value, _ = helper.rec_to_value(recommendation, recom_path, None)
    assert value == expected_val

# tests proper conversion of feature to index 
def test_feature_to_index():
    features_data = {
        "agro_climatic_zone": "Wet Kolla",
        "elevation":1002,
        "land_form": 8,
        "precipitation":500,
        "slope_shape":0,
        "soil_depth": "nodata",
        "soil_permeability":"nodata",
        "soil_texture": "clay",
        "topographic_slope": 34 
    }
    expected_index = {
        "agro_climatic_zone":4,
        "elevation":3,
        "land_form":4,
        "precipitation":1,
        "slope_shape":1,
        "soil_depth":-1,
        "soil_permeability":-1,
        "soil_texture":2,
        "topographic_slope":5
    }
    range_path =  environ.get("FEATURE_RANGES_FILE")
    returned_index = helper.feature_to_index(features_data, range_path)
    assert returned_index == expected_index

# tests the recommendation generator function
def test_recommend():
    recom_path = environ.get("RECOMMENDATION_FILE")
    range_path =  environ.get("FEATURE_RANGES_FILE")
    features_data = {
        "agro_climatic_zone": "Wet Kolla",
        "elevation":1002,
        "land_form": 8,
        "precipitation":500,
        "slope_shape":0,
        "soil_depth": "nodata",
        "soil_permeability":"nodata",
        "soil_texture": "clay",
        "topographic_slope": 34 
    }
    recommend = helper.recommend(features_data, recom_path, range_path)
    assert recommend == "Level soil bund"

def test_create_recommendation_file(test_raster): 
    recom_path = environ.get("RECOMMENDATION_FILE")
    range_path =  environ.get("FEATURE_RANGES_FILE")
    # The expected_raster numpy array is to be filled later
    expected_raster = np.array([[[255, 255, 255, 0], [255, 255, 255, 0], [255, 255, 255,0]]])
    raster_path, _ = test_raster
    helper.create_recommendation_file(raster_path, recom_path, range_path)
    
    with rasterio.open(raster_path, "r") as result_raster:
        result_array = result_raster.read()    
    #remove the test tiff created to test the current function
    os.remove(raster_path)
    assert np.array_equal(expected_raster, result_array)

def test_read_raster(test_raster):
    test_raster, _ = test_raster

    helper.read_raster(test_raster)
    
def test_write_raster(test_raster):
    _, raster_profile = test_raster
    sample_raster_path = "./test_write_raster.tiff"
    raster_grid = np.array([[[255, 255, 255],[255, 255, 255], [255, 255, 255]]])

    helper.write_raster(raster_grid, raster_profile, sample_raster_path)
    #removing the test tiff created to test the currents function
    os.remove(sample_raster_path)

def test_check():
    assert helper.check("dega", "agro_climatic_zone", "dega")
    assert helper.check(1, "land_form", [1, 2, 3])
    assert helper.check(5, "sample", ["i", 8])

    