import os
from flask_api import app
from flask import jsonify, request, json
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import text
from flask_cors import cross_origin


db = SQLAlchemy(app)

# to be updated
@app.route("/")
def index():

    return jsonify(
                version = "0.1"
            )

# get feature data at a specific lon and lat degree
@app.route("/get-data", methods = ["GET"])
@cross_origin(methods = ["GET"])
def get_data():

    lon = request.args.get("lon", type = float)
    lat = request.args.get("lat", type = float)

    if(type(lon) != float or type(lat) != float):
        return jsonify({"error" : "Bad Request. Non float value given."}), 400

    feature_values_query = text("SELECT * FROM rasters.get_feature_values(:lon, :lat)")
    # mask_value_query = text("SELECT * FROM rasters.get_aez_value(:lon, :lat)")

    feature_values = db.engine.execute(feature_values_query, lon=lon, lat=lat).fetchall()
    # mask_value = db.engine.execute(mask_value_query, lon=lon, lat=lat).fetchall()
    # result = {}
    # if(len(mask_value) > 0): mask_value = int(mask_value[0][1])
    result = {}
    # If there are data point returned from the dtatabse (i.e lon, lat are in bounds)
    if (len(feature_values) == 7): # number of parameters stored in the database
        for i in range(len(feature_values)):
            if (feature_values[i][1] is None):
                result[feature_values[i][0]] = "nodata"
            else:
                try:
                    result[feature_values[i][0]] = float(feature_values[i][1])
                except:
                    result[feature_values[i][0]] = feature_values[i][1]
    # The following is to add place holders for hamming distance calculation that will be done in the front end
    # Values that have not been retrieved from the database will be assigned -1 value 
    # The assignments in the following code must be removed once the corresponding data has been populated and hence can be accessed from the database
        result["soil_permeability"] = "nodata"
        result["soil_depth"] = "nodata"
        # result["mask"] = mask_value == 80 or mask_value == 50

    return jsonify(result)

# get encoded recommendation in json format
@app.route("/get-recommendation-data", methods = ["GET"])
@cross_origin(methods = ["GET"])
def get_recommendation_data():
    recommendation_json_file = os.path.join(app.static_folder, "data", "recommendations.json")
    with open(recommendation_json_file) as json_data:
        result = json.load(json_data)
    return result

# get sdg data from the database and returns to 
@app.route("/get-sdg-data", methods = ["GET"])
@cross_origin(methods = ["GET"])
def get_sdg():

    lon = request.args.get("lon", type = float)
    lat = request.args.get("lat", type = float)

    if(type(lon) != float or type(lat) != float):
        return jsonify({"error" : "Bad Request. Non float value given."}), 400

    sdg_values_query = text("SELECT * FROM diagnosis_data.get_sdg_value(:lon, :lat)")
    sdg_dict = db.engine.execute(sdg_values_query, lon=lon, lat=lat).fetchall()

    if(len(sdg_dict) == 1):
        sdg_value = int(sdg_dict[0][1])
        if(sdg_value == -1): return jsonify(result="degraded")
        elif(sdg_value == 0): return jsonify(result="stable")
        return jsonify(result="improvement")
    return {}
