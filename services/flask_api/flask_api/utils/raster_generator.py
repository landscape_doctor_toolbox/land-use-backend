import rasterio
from rasterio.mask import mask

# To be removed later 

# Script for creating a 1x2x3 tiff file

json_geom = {
  "type": "FeatureCollection",
  "features": [
    {
      "type": "Feature",
      "properties": {},
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              39.90,
              11.40
            ],
            [
              39.92,
              11.40
            ],
            [
              39.92,
              11.41
            ],
            [
              39.90,
              11.41
            ],
            [
              39.90,
              11.40
            ]
          ]
        ]
      }
    }
  ]
}

coords = [json_geom["features"][0]["geometry"]]
fname = "../static/raster/TAXOUSDA_1km_Ethiopia.tiff"
with rasterio.open(fname) as r:
    profile = r.profile
#   Clipping the tiff file based on Polygon
    A, out_transform = mask(r, coords, crop=True)

print(A.shape)
A = A.reshape(A.shape[1], A.shape[2])
#Updating profile
profile.update(dtype=rasterio.uint8, 
               driver="GTiff",
               height=A.shape[0],
               width=A.shape[1],
               transform=out_transform)

with rasterio.open("Test_recommendation.tif", 'w', **profile) as dst:
    dst.write(A.astype(rasterio.uint8), 1)