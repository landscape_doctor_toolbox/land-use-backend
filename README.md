# Land Use Recommendation

The land use recommendation system provides optimal land management techniques recommended by domain experts at [CIAT](https://cgspace.cgiar.org/handle/10568/89333).
The recommendations are provided from a list of 14 recommendations(SLM practices) by computing the closest distance using the parameters(Rainfall, slope shape, slope gradient, landform, soil depth, permeability, soil texture, altitudinal zone, and agro-climatic zone) as specified in [this excel sheet](https://drive.google.com/file/d/16_qAMVF81p2Xq5SrZ5hyJ_1M9xbEwyvB/view?usp=sharing). 

Implemented in a client-server architecture model, this repo includes the server side with the flask api and postgres database components for the land use recommendation system.
The parameters necessary for the recommendation generation have been converted into a database dump file and should be loaded into the database during the installation of the application. (See Installation section)
The information in the excel sheet has been converted into json format to ease calculation and stored int the static/data folder as recommendations.json. Its contents are also provided via the endpoint 'get-recommendation-data'.
A script to generate a map of Ethiopia containing recommendations on a 90mX90m scale has been included in the utils folder. It uses the feature_ranges.json file found in the static/utils folder to obtain the ranges of each subsection of the parameters found in the [Matching Guide excel sheet](https://drive.google.com/file/d/16_qAMVF81p2Xq5SrZ5hyJ_1M9xbEwyvB/view?usp=sharing). 

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

#### Docker 
[How to install on your machine](https://docs.docker.com/install/)

#### docker-compose 
[How to install on your machine](https://docs.docker.com/compose/install)



### Installing

A step by step series of examples that tell you how to get a development env running

Say what the step will be

```
git clone https://gitlab.com/icog-labs/agri/land_use_recommendation master

```
##### Obtain rasters.sql and place it in the ```services/postgres_db/overwrites/``` folder

```
cd land_use_recommendation

docker-compose up --build flask_api_dev -d

```

Access endpoints at 0.0.0.0:5500

#### Runing the generator
The script that can be used to generate the map containing the recommendations for Ethiopia can be run as the following.

```
docker exec -it <flask container name> bash

cd flask_api/utils

python3 generate.py --recommendationfile=../static/data/recommendations.json --rangefile=../static/data/feature_ranges.json --rasterfile=../static/data/raster_90.tif
```

## Running the tests

```
docker-compose up --build flask_api_test
```

## Deployment

A production container is available and can be run using:

```
docker-compose up --build -d flask_api_prod
```

## Built With

* [Flask](https://flask.palletsprojects.com/en/1.1.x/) - The web framework used
* [PostgreSQL Database](https://www.postgresql.org/docs/12/index.html) - Database used
* [PostGIS](https://postgis.net/2019/10/20/postgis-3.0.0/) - PostgreSQL Extention used
* [Docker](https://www.docker.com/) - Used to containerize backend components


## Authors

See the list of [contributors](https://gitlab.com/icog-labs/agri/land_use_recommendation/-/graphs/master) who participated in this project.
