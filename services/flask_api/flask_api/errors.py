from flask_api import app
from flask import jsonify


# 404 error handler
@app.errorhandler(404)
def handle_not_found_error(error):
    response = jsonify({
                    "success": False,
                    "error": {
                        "message": "Endpoint not found.",
                        "type": error.__class__.__name__,
                            }
                })
    return response, 404

# TODO: Internal server error handler
#       With respective test
