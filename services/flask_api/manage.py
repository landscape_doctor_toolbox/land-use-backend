from flask_api import configure_app


api = configure_app()

if __name__ == "__main__":
    api.run(host = "0.0.0.0", port = 5000)
