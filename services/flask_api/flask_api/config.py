from os import environ


class Config(object):
    ENV = environ.get("ENV")
    HOST = environ.get("SERVER_HOST", "0.0.0.0")
    try:
        PORT = int(environ.get("SERVER_PORT", "5000"))
    except ValueError:
        PORT = 5000

    DATABASE_URL = "postgresql://{postgres_user}:{postgres_pswd}@{postgres_host}:{postgres_port}/{postgres_db}".format(
        postgres_host = environ.get("POSTGRES_HOST"),
        postgres_user = environ.get("POSTGRES_USER"),
        postgres_pswd = environ.get("POSTGRES_PASSWORD"),
        postgres_port = int(environ.get("POSTGRES_PORT")),
        postgres_db = environ.get("APPLICATION_POSTGRES_DB")
    )
    SQLALCHEMY_DATABASE_URI = DATABASE_URL
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SECRET_KEY = environ.get("SECRET_KEY")
    CORS_ORIGINS = "*"

class ProductionConfig(Config):
    DEBUG = False
    TESTING = False
    CORS_ORIGINS = environ.get("CORS_ORIGINS")

class DevelopmentConfig(Config):
    DEBUG = True

class TestingConfig(Config):
    DEBUG = True
    TESTING = True
