import os
from flask import json
from flask_api import app
import pytest


# test the end point without sending longitude and latitude 
def test_index(client):
    response = client.get("/")
    json_data = response.get_json()
    assert json_data["version"] == "0.1"

# when the lon=36.73 and lat=8.95 is given
def test_get_data(client):
    lon = 36.73
    lat = 8.95
    response = client.get("/get-data?lon={lon}&lat={lat}".format(lon=lon, lat=lat))
    json_data = response.get_json()
    assert response.status_code == 200
    assert json_data["elevation"] == 1464.0
    assert json_data["precipitation"] == 1523.0534201152623
    assert json_data["soil_texture"] == "silt"
    assert json_data["topographic_slope"] == 89.98686981201172
    assert json_data["land_form"] == 8
    assert json_data["slope_shape"] == -2.567535193520598e-05
    assert json_data["agro_climatic_zone"] == "Wet Kolla"
    assert json_data["mask"] == False    

# when the given lon and lat degree is out of bounds
def test_get_data_out_of_bounds(client):
    lon = 3.0
    lat = 8.95
    response = client.get("/get-data?lon={lon}&lat={lat}".format(lon=lon, lat=lat))
    json_data = response.get_json()
    assert response.status_code == 200
    assert json_data == {}

# when the given lon and lat isn't of float type
def test_get_data_non_float_parameters(client):
    lon = 36
    lat = ""
    response = client.get("/get-data?lon={lon}&lat={lat}".format(lon=lon, lat=lat))
    assert "Bad Request. Non float value given." in response.get_json()["error"] 
    assert response.status_code == 400

# test 404 error handler
def test_not_found_error_handler(client):
    response = client.get("/non_existent_endpoint")
    assert response.get_json()["success"] == False
    assert "NotFound" in response.get_json()["error"]["type"]
    assert response.status_code == 404

# test 404 error handler
def test_get_recommendation_data(client):
    response = client.get("/get-recommendation-data")
    assert response.status_code == 200
    static_json_data = {}
    with open(os.path.join(app.static_folder, "data", "recommendations.json")) as recommendation_file:
        static_json_data = json.load(recommendation_file)
    assert response.get_json() == static_json_data
    
# test sdg endpoint when request includes longitude and latitude values
def test_sdg_data_endpoint(client):
    lon = 36.73
    lat = 8.95
    response = client.get("/get-sdg-data?lon={lon}&lat={lat}".format(lon=lon, lat=lat))
    json_data = response.get_json()
    assert response.status_code == 200
    assert json_data["result"] == "improvement"

# test sdg endpoint when request contains area out of bounds
def test_sdg_data_outofbounds(client):
    lon = 36.73
    lat = 19.0
    response = client.get("/get-sdg-data?lon={lon}&lat={lat}".format(lon=lon, lat=lat))
    json_data = response.get_json()
    assert response.status_code == 200
    assert json_data == {}
