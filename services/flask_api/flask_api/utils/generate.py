#!/usr/bin/python

"""
An example run, when the create_recommendation_file function is called:

    For instance, lets say we want to find a recommendation for a location at (lon, lat) = (36.73, 8.95)

    Inside create_recommendation_file function this goes on:
        1.  calls get_feature_data(lon, lat) and it should return feature data values

            feature_data = { "agro_climatic_zone":-1,
                            "elevation":1002,
                            "land_form":-1,
                            "precipitation":500,
                            "slope_shape":-1,
                            "soil_depth":-1,
                            "soil_permeability":-1,
                            "soil_texture":-1,
                            "topographic_slope":-1
                        }

        2. calls recommend(features_data, recom_path, range_path) function
            
            2.1 calls feature_to_index(features_data, range_path) function converts feature_data to feature index.
                For instance, for "elevation" : 1002, if 1002 is attribute on the third index of the elevation
                it will return "elevation": 3
                feature_index_pairs =  {
                        "agro_climatic_zone":-1,
                        "elevation":3,
                        "land_form":-1,
                        "precipitation":1,
                        "slope_shape":-1,
                        "soil_depth":-1,
                        "soil_permeability":-1,
                        "soil_texture":-1,
                        "topographic_slope":-1
                    }
            2.3 for each feature, based on the index, check if the value of the recommendation at the index is 1 or 0

                    Hamming distance between two lists [1, 1, 1] and [1, 0, 1] is 1.
                    The distance is determined by values that are different by 1.

                Hence if the value at the index is 0 or -1 the distance value for that recommendation is incremented.
                { "Hillside terrace" : 9, "Bench terrace" : 8 ... }

            return the recommendation with the smallest distance pair
"""

from helper import create_recommendation_file
import sys
import argparse


def main(argv):
    
    parser = argparse.ArgumentParser()
    parser.add_argument("--recommendationfile", required = True)
    parser.add_argument("--rangefile", required = True)
    parser.add_argument("--rasterfile", required = True)

    args = parser.parse_args()

    recom_path = args.recommendationfile
    range_path = args.rangefile
    raster_path =  args.rasterfile

    create_recommendation_file(raster_path, recom_path, range_path)


if __name__ == "__main__":
   main(sys.argv[1:])
