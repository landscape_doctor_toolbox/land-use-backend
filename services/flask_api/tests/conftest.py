from __future__ import absolute_import
import pytest
import os, sys
import rasterio
from rasterio.mask import mask

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))

from flask_api import configure_app, config

@pytest.fixture
def app():
    app = configure_app(config.TestingConfig())
    yield app

@pytest.fixture
def client(app):
    return app.test_client()

# a fixture function that generates a test raster file by cropping RASTER_FILE
# returns the path to the test raster file and its corresponding profile
@pytest.fixture
def test_raster():
    raster_path = os.getenv("RASTER_FILE")
    coords = [{"type": "Polygon", 
    "coordinates": [[[37.22,8.71],[37.225,8.71],[37.225,8.715],[37.22,8.715],[37.22,8.71]]]}]
    with rasterio.open(raster_path, "r") as raster:
        profile = raster.profile
    #   Clipping the tiff file based on Polygon
        test_raster, out_transform = mask(raster, coords, crop=True)

    #Updating profile    
    profile.update(driver="GTiff",
                height=test_raster.shape[1],
                width=test_raster.shape[2],
                transform=out_transform)
    try:
        with rasterio.open("test_recom.tif", 'w', **profile) as dst:
            dst.write(test_raster)
    except:
        print("Couldn't write file")
        exit(1)
    return os.path.join(os.path.abspath('./'), "test_recom.tif"), profile
