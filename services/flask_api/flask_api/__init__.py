from flask import Flask
from flask_api import config


app = Flask(__name__)
app.config.from_object(config.Config())

import flask_api.errors
import flask_api.routes

def configure_app(configuration = config.Config()):
    if configuration.ENV == "development":
        configuration = config.DevelopmentConfig()
    elif configuration.ENV == "testing":
        configuration = config.TestingConfig()
    elif configuration.ENV == "production":
        configuration = config.ProductionConfig()
    app.config.from_object(configuration)

    return app
   