#!/bin/sh

if [ "$DATABASE" = "postgres" ]
then
    echo "Waiting for postgres..."

    while ! nc -z $SQL_HOST $SQL_PORT; do
      sleep 0.1
    done

    echo "PostgreSQL started"
fi

if [ "$ENV" = "development" ];
then
    python3 manage.py

elif [ "$ENV" = "testing" ];
then
  pytest

elif [ "$ENV" = "production" ];
then
  gunicorn --bind 0.0.0.0:5000 manage:api
  
fi

exec "$@"
